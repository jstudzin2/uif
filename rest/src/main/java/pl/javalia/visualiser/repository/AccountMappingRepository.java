package pl.javalia.visualiser.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import pl.javalia.visualiser.model.AccountMapping;

/**
 * Place description here.
 *
 */

@RepositoryRestResource(collectionResourceRel = "resources", path = "accounts")
public interface AccountMappingRepository extends PagingAndSortingRepository<AccountMapping, Long> {

    @RestResource(exported = true)
    @Query("select accountMapping from AccountMapping accountMapping where " +
            "LOWER(accountMapping.syssec) like LOWER(:search||'%') or LOWER(accountMapping.sdesc) like LOWER(:search||'%')" +
            " or LOWER(accountMapping.tt) like LOWER(:search||'%')  or LOWER(accountMapping.ldesc) like LOWER(:search||'%')" +
            " or LOWER(accountMapping.acct) like LOWER(:search||'%')  or LOWER(accountMapping.leg) like LOWER(:search||'%')" +
            "  or LOWER(accountMapping.ldesc1) like LOWER(:search||'%') ORDER BY ldesc,syssec,tt,leg")
    Page<AccountMapping> findAllLike(@Param("search") String search, Pageable pageable);

}