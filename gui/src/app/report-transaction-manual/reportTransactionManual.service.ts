import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Response, URLSearchParams} from '@angular/http';
import {environment} from '../../environments/environment';
import {RestResponse} from '../model/rest-response';
import {UifHttp} from '../model/uif-http';
import 'rxjs/add/operator/map';
import {ReportTransation} from '../report-transaction/reportTransation';

const SYSTEM_PREFIX = environment.BACKEND;
const PARAM1 = 'transactions';
const PARAM2 = 'transactionsBank';
const PARAM3 = 'transactionsExped';

@Injectable()
export class ReportTransationManualService {

  constructor(private http: UifHttp) {
  }

  list(query:string,recon:boolean,page = 0, sort = 'created', direction = 'desc', file:number): Observable<RestResponse<ReportTransation>> {
    let listUrl = SYSTEM_PREFIX + this.getParam(file)+ "/search/findAllLike";
    if(recon){
        listUrl = listUrl+"Recon";
    }
    const params = new URLSearchParams();
    params.set('page', page.toString());
    if(query) {
      params.set('search', query.replace('.', ','));
    }
    params.set('sort', sort + ',' + direction);
    return this.http.get(listUrl, {search: params}).map(response => response.json() as RestResponse<ReportTransation>);
  }

  edit(reportTransation: ReportTransation, file:number): Observable<Response> {
    const updateUrl = SYSTEM_PREFIX + this.getParam(file) + '/' + reportTransation.id;
    return this.http.put(updateUrl, reportTransation);
  }

  add(reportTransation: ReportTransation, file:number): Observable<Response> {
    const addUrl = SYSTEM_PREFIX + this.getParam(file);
    return this.http.post(addUrl, reportTransation);
  }

  delete(reportTransation: ReportTransation, file:number): Observable<Response> {
    const deleteUrl = SYSTEM_PREFIX + this.getParam(file) + '/' + reportTransation.id;
    return this.http.delete(deleteUrl);
  }

  getParam(report: number): any {
    if(report==1){
        return PARAM1;
    }
    if(report==2){
        return PARAM2;
    }
    if(report==3){
        return PARAM3;
    }
    return PARAM1;
  }

}
