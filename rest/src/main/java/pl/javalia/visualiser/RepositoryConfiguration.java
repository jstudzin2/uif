package pl.javalia.visualiser;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import pl.javalia.visualiser.model.*;
import pl.javalia.visualiser.model.Exception;

/**
 * Created by Kuba&Sylwia on 09.02.18.FILES
 */
@Configuration
public class RepositoryConfiguration extends RepositoryRestConfigurerAdapter {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Accruals.class, TotalReversals.class, AccountControl.class, GenexToCreate.class, GenexTotalWa4.class, GenexTotalWa5.class, GenexTotalWa2.class, GenexTotalWa6.class, AccountMapping.class, GenexFile.class, GenexStatus.class, GroupMapping.class, TtMapping.class, SumTotals.class,
                Parameter.class, ProcessError.class, ProcessValidation.class, ReportTransaction.class, Total.class, Status.class,
                ReportTransactionBank.class, Duplicate.class, AuditChangeLog.class, AuditFile.class, Exception.class, GenexTotalWa5.class, ReportTransactionBroker.class, ReportTransactionExped.class);
        config.setDefaultPageSize(5);
    }
}
