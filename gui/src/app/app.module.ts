import {BrowserModule} from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import {AppComponent} from './app.component';
import {TopMenuComponent} from './top-menu/top-menu.component';
import {AppRoutingModule} from './app-routing.module';
import {ConfigComponent} from './config/config.component';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {ParameterComponent} from './parameter/parameter.component';
import {GroupComponent} from './group/group.component';
import {AccountComponent} from './account/account.component';
import {GenexComponent} from './genex/genex.component';
import {ReconciliationComponent} from './reconciliation/reconciliation.component';
import {ProcessComponent} from './process/process.component';
import {GenexStatusComponent} from './genex-status/genex-status.component';
import {ProcessErrorComponent} from './process-error/process-error.component';
import {ProcessValidationComponent} from './process-validation/process-validation.component';
import {RunProcessComponent} from './run-process/run-process.component';
import {TotalsComponent} from './totals/totals.component';
import {ReportTransactionComponent} from './report-transaction/report-transaction.component';
import {UifHttp} from './model/uif-http';
import {AccountService} from './account/account.service';
import {ParameterService} from './parameter/parameter.service';
import {GroupService} from './group/group.service';
import {GenexService} from './genex/genex.service';
import {ReportTransationService} from './report-transaction/reportTransaction.service';
import {ReportTransationManualService} from './report-transaction-manual/reportTransactionManual.service';
import {ProcessErrorService} from './process-error/process-error.service';
import {ProcessValidationService} from './process-validation/process-validation.service';
import {GenexStatusService} from './genex-status/genex-status.service';
import {TotalsService} from './totals/totals.service';
import {StatusService} from './run-process/status.service';
import {ReportTransactionManualComponent} from './report-transaction-manual/report-transaction-manual.component';
import {ModalModule} from 'ngx-modal';
import {TtmappingComponent} from './ttmapping/ttmapping.component';
import {TtmappingService} from "./ttmapping/ttmapping.service";
import {MappingComponent} from './mapping/mapping.component';
import {SumtotalsComponent} from './sumtotals/sumtotals.component';
import {SumTotalsService} from "./sumtotals/sumtotals.service";
import {ExceptionsComponent} from './exceptions/exceptions.component';
import {AuditComponent} from './audit/audit.component';
import {ExceptionsService} from "./exceptions/exceptions.service";
import {ExceptionPaneComponent} from './exception-pane/exception-pane.component';
import {AuditFilesComponent} from './audit-files/audit-files.component';
import {AuditFileService} from "./audit-files/auditFile.service";
import {AuditChangeLogComponent} from './audit-change-log/audit-change-log.component';
import {AuditChangeLogServce} from "./audit-change-log/audit-change-log-servce";
import {DuplicatesComponent} from './duplicates/duplicates.component';
import {DuplicateService} from "./duplicates/duplicate.service";
import {StatisticPaneComponent} from './statistic-pane/statistic-pane.component';
import {GenexTotalsWas5Component} from './genex-totals-was5/genex-totals-was5.component';
import {GenexTotalsWas6Component} from './genex-totals-was6/genex-totals-was6.component';
import {GenexTotalsWas5Service} from "./genex-totals-was5/genex-totals-was5.service";
import {GenexTotalsWas6Service} from "./genex-totals-was6/genex-totals-was6.service";
import { GenexTotalsWas2Component } from './genex-totals-was2/genex-totals-was2.component';
import { GenexTotalsWas4Component } from './genex-totals-was4/genex-totals-was4.component';
import {GenexTotalsWas2Service} from "./genex-totals-was2/genex-totals-was2.service";
import {GenexTotalsWas4Service} from "./genex-totals-was4/genex-totals-was4.service";
import { ControlsComponent } from './controls/controls.component';
import { AccountControlComponent } from './account-control/account-control.component';
import { GenexToCreateComponent } from './genex-to-create/genex-to-create.component';
import {GenexToCreateService} from "./genex-to-create/genex-to-create.service";
import {AccountControlService} from "./account-control/account-control.service";
import { AccrualsComponent } from './accruals/accruals.component';
import {AccrualsService} from "./accruals/accruals.service";
import { TotalReversalsComponent } from './total-reversals/total-reversals.component';
import {TotalReversalsService} from "./total-reversals/total-reversals.service";
import localeEnZA from '@angular/common/locales/en-zA';
import {registerLocaleData} from "@angular/common";
import {CookieModule} from "ngx-cookie";
import { UnauthorizedComponent } from './unauthorized/unauthorized.component';

registerLocaleData(localeEnZA, 'enZA');

@NgModule({
  declarations: [
    AppComponent,
    TopMenuComponent,
    ConfigComponent,
    ParameterComponent,
    GroupComponent,
    AccountComponent,
    GenexComponent,
    ReconciliationComponent,
    ProcessComponent,
    GenexStatusComponent,
    ProcessErrorComponent,
    ProcessValidationComponent,
    RunProcessComponent,
    TotalsComponent,
    ReportTransactionComponent,
    ReportTransactionManualComponent,
    TtmappingComponent,
    MappingComponent,
    SumtotalsComponent,
    ExceptionsComponent,
    AuditComponent,
    ExceptionPaneComponent,
    AuditFilesComponent,
    AuditChangeLogComponent,
    DuplicatesComponent,
    StatisticPaneComponent,
    GenexTotalsWas5Component,
    GenexTotalsWas6Component,
    GenexTotalsWas2Component,
    GenexTotalsWas4Component,
    ControlsComponent,
    AccountControlComponent,
    GenexToCreateComponent,
    AccrualsComponent,
    TotalReversalsComponent,
    UnauthorizedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    ModalModule,
    CookieModule.forRoot()
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'enZA' },
    UifHttp,
    TotalReversalsService,
    AccountService,
    AccrualsService,
    GenexToCreateService,
    AccountControlService,
    ParameterService,
    GenexService,
    GenexTotalsWas5Service,
    GenexTotalsWas6Service,
    GenexTotalsWas2Service,
    GenexTotalsWas4Service,
    ReportTransationService,
    SumTotalsService,
    TtmappingService,
    AuditFileService,
    ReportTransationManualService,
    DuplicateService,
    ProcessErrorService,
    AuditChangeLogServce,
    ProcessValidationService,
    GenexStatusService,
    ExceptionsService,
    StatusService,
    TotalsService,
    GroupService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
