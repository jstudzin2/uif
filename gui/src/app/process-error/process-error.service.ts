///<reference path="../model/uif-http.ts"/>
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {URLSearchParams} from '@angular/http';
import {environment} from '../../environments/environment';
import {RestResponse} from '../model/rest-response';
import {UifHttp} from '../model/uif-http';
import 'rxjs/add/operator/map';
import {ProcessError} from './processError';

const SYSTEM_PREFIX = environment.BACKEND;
const PARAM = 'errors';

@Injectable()
export class ProcessErrorService {

  constructor(private http: UifHttp) {
  }

  list(query:string,page = 0, sort = 'created', direction = 'desc'): Observable<RestResponse<ProcessError>> {
    const listUrl = SYSTEM_PREFIX + PARAM+ "/search/findAllLike";
    const params = new URLSearchParams();
    params.set('page', page.toString());
    params.set('sort', sort + ',' + direction);
    if(query) {
      params.set('search', query.replace('.', ','));
    }
    return this.http.get(listUrl, {search: params}).map(response => response.json() as RestResponse<ProcessError>);
  }


}
