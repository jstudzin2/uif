export class GenexStatus {
  date: string;
  description: string;
  system: string;
}
