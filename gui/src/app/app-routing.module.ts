import {ConfigComponent} from './config/config.component';
import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ReconciliationComponent} from './reconciliation/reconciliation.component';
import {ProcessComponent} from './process/process.component';
import {TtmappingComponent} from "./ttmapping/ttmapping.component";
import {MappingComponent} from "./mapping/mapping.component";
import {AuditComponent} from "./audit/audit.component";
import {ExceptionsComponent} from "./exceptions/exceptions.component";
import {ExceptionPaneComponent} from "./exception-pane/exception-pane.component";
import {StatisticPaneComponent} from "./statistic-pane/statistic-pane.component";
import {ControlsComponent} from "./controls/controls.component";
import {UnauthorizedComponent} from "./unauthorized/unauthorized.component";


const routes: Routes = [
  {path: '', redirectTo: '/process', pathMatch: 'full'},
  {path: 'config', component: ConfigComponent},
  {path: 'process', component: ProcessComponent},
  {path: 'ttmapping', component: MappingComponent},
  {path: 'unauthorized', component: UnauthorizedComponent},
  {path: 'reconciliation', component: ReconciliationComponent},
  {path: 'audit', component: AuditComponent},
  {path: 'statistics', component: StatisticPaneComponent},
  {path: 'controls', component: ControlsComponent},
  {path: 'exceptions', component: ExceptionsComponent},
  {path: 'exceptionPane', component: ExceptionPaneComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
