import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptionsArgs, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UifHttp {

  constructor(private http: Http) {
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    const observable: Observable<Response> = this.http.get(url, options);
    return observable;
  }

  post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    const observable: Observable<Response> = this.http.post(url, body, options || this.defaultPostOptions());
    return observable;
  }

  put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    const observable: Observable<Response> = this.http.put(url, body, options || this.defaultPostOptions());
    return observable;
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    const observable: Observable<Response> = this.http.delete(url, options);
    return observable;
  }


  defaultPostOptions(): RequestOptionsArgs {
    const options: RequestOptionsArgs = {};
    const headers: Headers = new Headers({'Content-Type': 'application/json'});
    options.headers = headers;
    return options;
  }

}

