// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  SYSTEM_NAME: 'UIF/',
  // BACKEND: 'http://localhost:8080/'
  // BACKEND: 'http://10.128.96.234:8080/'
  BACKEND: 'http://172.20.21.154:8080/'
};
