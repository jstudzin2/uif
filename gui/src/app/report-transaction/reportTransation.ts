export class ReportTransation {
  id: number;
  reconId: number;
  manualId: number;
  security: string;
  amount: number;
  groupId: string;
  skip: string;
  date: Date;
  description: string;
  portfolio: string;
  reconFlag:string;
  gnxFlag:string;
  tt:string;
  mtt:string;

  workAmount:number;
  brAmount:number;
  proceeds:number;
  totalExp:number;
  costAmount:number;
  allin:number;
  prolo:number;

  edition = false;
  new = false;
}
