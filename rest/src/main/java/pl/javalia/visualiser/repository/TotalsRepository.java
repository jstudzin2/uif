package pl.javalia.visualiser.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.javalia.visualiser.model.ProcessValidation;
import pl.javalia.visualiser.model.Total;

/**
 * Place description here.
 *
 * @author Q1JS@nykredit.dk
 */

@RepositoryRestResource(collectionResourceRel = "resources", path = "totals")
public interface TotalsRepository extends PagingAndSortingRepository<Total, Long> {

}