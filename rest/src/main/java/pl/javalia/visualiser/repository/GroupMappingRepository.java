package pl.javalia.visualiser.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import pl.javalia.visualiser.model.GenexFile;
import pl.javalia.visualiser.model.GenexStatus;
import pl.javalia.visualiser.model.GroupMapping;

/**
 * Place description here.
 *
 */

@RepositoryRestResource(collectionResourceRel = "resources", path = "groups")
public interface GroupMappingRepository extends PagingAndSortingRepository<GroupMapping, Long> {

    @RestResource(exported = true)
    @Query("select groupMapping from GroupMapping groupMapping where " +
            "LOWER(groupMapping.tt) like LOWER(:search||'%') or LOWER(groupMapping.ttype) like LOWER(:search||'%')" +
            " or LOWER(groupMapping.source) like LOWER(:search||'%')")
    Page<GroupMapping> findAllLike(@Param("search") String search, Pageable pageable);
}