import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Genex} from './genex';
import {Response, URLSearchParams} from '@angular/http';
import {environment} from '../../environments/environment';
import {RestResponse} from '../model/rest-response';
import {UifHttp} from '../model/uif-http';
import 'rxjs/add/operator/map';

const SYSTEM_PREFIX = environment.BACKEND ;
const PARAM = 'files' ;
@Injectable()
export class GenexService {

  constructor(private http: UifHttp) {
  }

  list(query:string,page = 0, sort = 'created', direction = 'desc'): Observable<RestResponse<Genex>> {
    const listUrl = SYSTEM_PREFIX + PARAM+ "/search/findAllLike";
    const params = new URLSearchParams();
    params.set('page', page.toString());
    params.set('sort', sort + ',' + direction);
    if(query) {
      params.set('search', query.replace('.', ','));
    }
    return this.http.get(listUrl,{search:params}).map(response => response.json() as RestResponse<Genex>);
  }

  edit(genex: Genex): Observable<Response> {
    const updateUrl = SYSTEM_PREFIX + PARAM+"/"+genex.id;
    return this.http.put(updateUrl, genex);
  }

  add(genex: Genex): Observable<Response> {
    const addUrl = SYSTEM_PREFIX + PARAM;
    return this.http.post(addUrl, genex);
  }

  delete(genex: Genex): Observable<Response> {
    const deleteUrl = SYSTEM_PREFIX + PARAM+"/"+genex.id;
    return this.http.delete(deleteUrl);
  }

  csv():any{
    return this.http.get(SYSTEM_PREFIX+"csv/genex");
  }

}
