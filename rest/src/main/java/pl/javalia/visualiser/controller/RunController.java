package pl.javalia.visualiser.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.javalia.visualiser.repository.StatusRepository;
import pl.javalia.visualiser.service.RunService;

/**
 * Created by Kuba&Sylwia on 11.02.18.
 */
@RestController
public class RunController {

    @Autowired
    private RunService runService;

    @PostMapping("/status/{processNo}/run")
    public String run(@PathVariable(value = "processNo") Integer processNo) {
        return runService.run(processNo);
    }

    @PostMapping("/status/{processNo}/run/{param}")
    public String runWithArgument(@PathVariable(value = "processNo") Integer processNo,
                                  @PathVariable(value = "param") String param) {
        return runService.runWithArgument(processNo, param);
    }

    @PostMapping("/status/{processNo}/run/{sysid}/{syssec}/{SOURCE}/{org}/{map}/{portfolio}/{GNX_FLAG}")
    public String runWithArguments(@PathVariable(value = "processNo") Integer processNo,
                                   @PathVariable(value = "sysid") String sysid,
                                   @PathVariable(value = "syssec") String syssec,
                                   @PathVariable(value = "SOURCE") String SOURCE,
                                   @PathVariable(value = "org") String org,
                                   @PathVariable(value = "map") String map,
                                   @PathVariable(value = "portfolio") String portfolio,
                                   @PathVariable(value = "GNX_FLAG") String GNX_FLAG) {
        return runService.runWithArguments(processNo, sysid, syssec, SOURCE, org, map, portfolio, GNX_FLAG);
    }
}
