import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Response} from '@angular/http';
import {environment} from '../../environments/environment';
import {RestResponse} from '../model/rest-response';
import {UifHttp} from '../model/uif-http';
import 'rxjs/add/operator/map';
import {Status} from './status';
import {Exceptions} from "../exceptions/exceptions";

const SYSTEM_PREFIX = environment.BACKEND;
const PARAM = 'status';

@Injectable()
export class StatusService {

  constructor(private http: UifHttp) {
  }

  list(sa: boolean): Observable<RestResponse<Status>> {
    let listUrl = SYSTEM_PREFIX + PARAM + "/search/findAll?standAlone="+sa;
    return this.http.get(listUrl).map(response => response.json() as RestResponse<Status>);
  }

  run(status: Status): Observable<Response> {
    const updateUrl = SYSTEM_PREFIX + PARAM + '/' + status.processId + "/run";
    return this.http.post(updateUrl, status);
  }

  runWithException(processId: number, exception: Exceptions): Observable<Response> {
    const updateUrl = SYSTEM_PREFIX + PARAM + '/' + processId + "/run/" +
      exception.id + "/" +
      exception.security + "/" +
      exception.source + "/" +
      exception.tt + "/" +
      exception.mtt + "/" +
      exception.portfolio + "/" +
      exception.gnxFlag;
    return this.http.post(updateUrl, status);
  }

  runWithParam(status: Status, param: string): Observable<Response> {
    const updateUrl = SYSTEM_PREFIX + PARAM + '/' + status.processId + "/run/" + param;
    return this.http.post(updateUrl, status);
  }

  runById(processId: number): Observable<Response> {
    const updateUrl = SYSTEM_PREFIX + PARAM + '/' + processId + "/run";
    return this.http.post(updateUrl, status);
  }
}
