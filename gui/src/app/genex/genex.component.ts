import {Component, OnInit, ViewChild} from '@angular/core';
import {Genex} from './genex';
import {GenexService} from './genex.service';
import {Page} from '../model/page';
import {SortConfig} from '../model/sort-config';
import {Modal} from "ngx-modal";
import {StatusService} from "../run-process/status.service";

@Component({
  selector: 'app-genex',
  templateUrl: './genex.component.html',
  styleUrls: ['./genex.component.css']
})
export class GenexComponent implements OnInit {

  public genexes: Genex[] = [];
  editCopy: Genex;
  public query: string;
  public page: Page = null;
  public sortConfig = new SortConfig('date', this);
  @ViewChild('myModal') modal: Modal;
  public running: boolean = false;

  constructor(private genexService: GenexService,private statusService: StatusService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData(page?: number): void {
    this.genexService.list(this.query, page, this.sortConfig.field, this.sortConfig.direction)
      .subscribe(
        response => {
          this.genexes = [];
          const genexes = response._embedded ? response._embedded.resources : [];
          for (const genex of genexes) {
            this.genexes.push(genex);
          }
          this.page = response.page;
        }
      );
  }

  numberWithCommas(x): string {
    return x.toString().replace(",", ".");
  }

  edit(genex: Genex) {
    this.editCopy = new Genex();
    this.editCopy.date = genex.date;
    this.editCopy.description = genex.description;
    this.editCopy.amount = genex.amount;
    this.editCopy.account = genex.account;
    genex.edition = true;
  }

  delete(genex: Genex) {
    this.genexService.delete(genex).subscribe(() => this.getData());
  }

  cancel(genex: Genex) {
    if (genex.new) {
      this.genexes = this.genexes.filter((gen) => !gen.new);
    } else {
      genex.description = this.editCopy.description;
      genex.date = this.editCopy.date;
      genex.amount = this.editCopy.amount;
      genex.account = this.editCopy.account;
      genex.edition = false
    }
  }

  save(genex: Genex) {
    if (genex.new) {
      this.genexService.add(genex).subscribe(() => this.getData());
    } else {
      this.genexService.edit(genex).subscribe(() => this.getData());
    }
  }

  add() {
    const genex = new Genex;
    genex.edition = true;
    genex.new = true;
    this.genexes.push(genex);
  }

  changePage(toPage: string): void {
    if (toPage !== '...') {
      this.getData(+toPage - 1);
    }
  }

  getPages(): string[] {
    const possibleChoices: string[] = ['1'];

    if (this.page.totalPages > 1) {
      if (this.page.totalPages > 5) {
        if (this.page.number > 1 && this.page.number < this.page.totalPages - 2) {
          const currentPage = this.page.number + 1;
          possibleChoices.push('...');
          possibleChoices.push(this.page.number.toString());
          possibleChoices.push(currentPage.toString());
          possibleChoices.push((currentPage + 1).toString());
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else if (this.page.number < this.page.totalPages - 2) {
          possibleChoices.push('2');
          possibleChoices.push('3');
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else {
          possibleChoices.push('2');
          possibleChoices.push('...');
          possibleChoices.push((this.page.totalPages - 2).toString());
          possibleChoices.push((this.page.totalPages - 1).toString());
          possibleChoices.push(this.page.totalPages.toString());
        }
      } else {
        for (let i = 1; i < this.page.totalPages; i++) {
          possibleChoices.push((i + 1).toString());
        }
      }
    }

    return possibleChoices;
  }

  downloadCSV() {
    var nameOfFileToDownload = "GenexMappings.csv";
    this.genexService.csv().subscribe(
      success => {
        var blob = new Blob([success._body], {type: 'text/csv'});

        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(blob, nameOfFileToDownload);
        } else {
          var a = document.createElement('a');
          a.href = URL.createObjectURL(blob);
          a.download = nameOfFileToDownload;
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
        }
      },
      err => {
        alert("Server error while downloading file.");
      }
    );
  }


  confirm() {
    this.modal.open();
  }

  run() {
    this.running = true;
    this.statusService.runById(6).subscribe(
      response=>{
        this.getData();
        this.running=false;
        this.modal.close();
      }
    );
  }


}
