package pl.javalia.visualiser.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

/**
 * Place description here.
 *
 * @author Q1JS@nykredit.dk
 */
@Entity
@Table(name = "SG3_536_ACCT_MAP_WORKAREA")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountMapping {

    @Id
    @Column(name = "SYSID")
    @GeneratedValue(strategy = GenerationType.AUTO, generator="SG3_536_ACCT")
    @SequenceGenerator(name="SG3_536_ACCT", sequenceName="SG3_536_ACCT")
    private Long id;

    @Length(max = 16)
    @Column(name = "SYSSEC")
    private String syssec;

    @Length(max = 36)
    @Column(name = "SDESC")
    private String sdesc;

    @Length(max = 10)
    @Column(name = "TT")
    private String tt;

    @Length(max = 80)
    @Column(name = "LDESC")
    private String ldesc;

    @Length(max = 16)
    @Column(name = "ACCT")
    private String acct;

    @Length(max = 10)
    @Column(name = "LEG")
    private String leg;

    @Column(name = "LDESC1")
    @Length(max = 20)
    private String ldesc1;
}
