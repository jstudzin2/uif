import {Component, OnInit} from '@angular/core';
import {ProcessValidation} from './processValidation';
import {Page} from '../model/page';
import {SortConfig} from '../model/sort-config';
import {ProcessValidationService} from './process-validation.service';

@Component({
  selector: 'app-process-validation',
  templateUrl: './process-validation.component.html',
  styleUrls: ['./process-validation.component.css']
})
export class ProcessValidationComponent implements OnInit {

  public processValidations: ProcessValidation[] = [];

  public query:string;
  public page: Page = null;
  public sortConfig = new SortConfig('source', this);

  constructor(private processValidationService: ProcessValidationService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData(page?: number): void {
    this.processValidationService.list(this.query,page, this.sortConfig.field, this.sortConfig.direction)
      .subscribe(
        response => {
          this.processValidations = [];
          const processValidations = response._embedded ? response._embedded.resources : [];
          for (const processValidation of processValidations) {
            this.processValidations.push(processValidation);
          }
          this.page = response.page;
        }
      );
  }


  getPages(): string[] {
    const possibleChoices: string[] = ['1'];

    if (this.page.totalPages > 1) {
      if (this.page.totalPages > 5) {
        if (this.page.number > 1 && this.page.number < this.page.totalPages - 2) {
          const currentPage = this.page.number + 1;
          possibleChoices.push('...');
          possibleChoices.push(this.page.number.toString());
          possibleChoices.push(currentPage.toString());
          possibleChoices.push((currentPage + 1).toString());
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else if (this.page.number < this.page.totalPages - 2) {
          possibleChoices.push('2');
          possibleChoices.push('3');
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else {
          possibleChoices.push('2');
          possibleChoices.push('...');
          possibleChoices.push((this.page.totalPages - 2).toString());
          possibleChoices.push((this.page.totalPages - 1).toString());
          possibleChoices.push(this.page.totalPages.toString());
        }
      } else {
        for (let i = 1; i < this.page.totalPages; i++) {
          possibleChoices.push((i + 1).toString());
        }
      }
    }

    return possibleChoices;
  }

  changePage(toPage: string): void {
    if (toPage !== '...') {
      this.getData(+toPage - 1);
    }
  }
}
