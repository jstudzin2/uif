package pl.javalia.visualiser.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import pl.javalia.visualiser.model.GenexStatus;
import pl.javalia.visualiser.model.ReportTransaction;

import java.util.Date;

/**
 * Place description here.
 *
 */

@RepositoryRestResource(collectionResourceRel = "resources", path = "genexes")
public interface GenexStatusRepository extends PagingAndSortingRepository<GenexStatus, String> {

    @RestResource(exported = true)
    @Query("select genexStatus from GenexStatus genexStatus where " +
            "LOWER(genexStatus.description) like LOWER(:search||'%') or LOWER(genexStatus.system) like LOWER(:search||'%')" +
            " or LOWER(genexStatus.date) like LOWER(:search||'%') ORDER BY genexStatus.date desc")
    Page<GenexStatus> findAllLike(@Param("search") String search, Pageable pageable);

}