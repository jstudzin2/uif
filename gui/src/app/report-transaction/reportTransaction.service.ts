import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Response, URLSearchParams} from '@angular/http';
import {environment} from '../../environments/environment';
import {RestResponse} from '../model/rest-response';
import {UifHttp} from '../model/uif-http';
import 'rxjs/add/operator/map';
import {ReportTransation} from './reportTransation';

const SYSTEM_PREFIX = environment.BACKEND;
const PARAM = 'transactionsBroker';

@Injectable()
export class ReportTransationService {

  constructor(private http: UifHttp) {
  }

  list(query:string,recon:boolean,page = 0, sort = 'created', direction = 'desc'): Observable<RestResponse<ReportTransation>> {
    let listUrl = SYSTEM_PREFIX + PARAM + "/search/findAllLike";
    if(recon){
        listUrl = listUrl+"Recon";
    }
    const params = new URLSearchParams();
    params.set('page', page.toString());
    if(query) {
      params.set('search', query.replace('.', ','));
    }
    params.set('sort', sort + ',' + direction);
    return this.http.get(listUrl, {search: params}).map(response => response.json() as RestResponse<ReportTransation>);
  }

  edit(reportTransation: ReportTransation): Observable<Response> {
    const updateUrl = SYSTEM_PREFIX + PARAM + '/' + reportTransation.id;
    return this.http.put(updateUrl, reportTransation);
  }

  add(reportTransation: ReportTransation): Observable<Response> {
    const addUrl = SYSTEM_PREFIX + PARAM;
    return this.http.post(addUrl, reportTransation);
  }

  delete(reportTransation: ReportTransation): Observable<Response> {
    const deleteUrl = SYSTEM_PREFIX + PARAM + '/' + reportTransation.id;
    return this.http.delete(deleteUrl);
  }

}
