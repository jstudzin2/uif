import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessErrorComponent } from './process-error.component';

describe('ProcessErrorComponent', () => {
  let component: ProcessErrorComponent;
  let fixture: ComponentFixture<ProcessErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
