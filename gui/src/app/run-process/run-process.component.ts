import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {StatusService} from './status.service';
import {Status} from './status';
import {Modal} from "ngx-modal";

@Component({
  selector: 'app-run-process',
  templateUrl: './run-process.component.html',
  styleUrls: ['./run-process.component.css']
})
export class RunProcessComponent implements OnInit {

  public statuses: Status[] = [];
  @ViewChild('myModal') modal: Modal;
  public status: Status;
  public running: boolean = false;
  public period: string;

  @Input()
  public sa: boolean = false;

  constructor(private statusService: StatusService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData(): void {
    this.statusService.list(this.sa)
      .subscribe(
        response => {
          this.statuses = [];
          const statuses = response._embedded ? response._embedded.resources : [];
          for (const status of statuses) {
            this.statuses.push(status);
          }
        }
      );
  }

  getProcessName(status: Status) {
    if (status.name) {
      return status.name;
    }
    const processNo = status.processId;
    if (processNo == 0) {
      return "Clean all tables";
    }
    if (processNo == 1) {
      return "Run auto reconciliation";
    }
    if (processNo == 2) {
      return "Run Edit Genex Only";
    }
    if (processNo == 3) {
      return "Run Update Genex and GL Post Edit Report";
    }
    if (processNo == 8) {
      return "Setup Posting Period";
    }
    if (processNo == 9) {
      return "Load reversals";
    }
    if (processNo == 4) {
      return "Run manual reconciliation process";
    }
    if (processNo == 90) {
      return "Reload expenditures/invoices";
    }
    return "Unknown";
  }

  set(statusToSet: Status) {
    this.status = statusToSet;
  }

  run() {
    this.status.running = true;
    if (this.period) {
      this.statusService.runWithParam(this.status, this.period).subscribe(
        response => {
          this.getData();
          this.running = false;
          this.modal.close();
        }
      )
    } else {
      this.statusService.run(this.status).subscribe(
        response => {
          this.getData();
          this.running = false;
          this.modal.close();
        }
      )
    }
  }

  refresh() {
    this.getData();
  }

}
