import {Component, OnInit} from '@angular/core';
import {Totals} from './totals';
import {Page} from '../model/page';
import {SortConfig} from '../model/sort-config';
import {TotalsService} from './totals.service';

@Component({
  selector: 'app-totals',
  templateUrl: './totals.component.html',
  styleUrls: ['./totals.component.css']
})
export class TotalsComponent implements OnInit {

  public totals: Totals[] = [];

  public page: Page = null;
  public sortConfig = new SortConfig('source', this);

  constructor(private totalsService: TotalsService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData(page?: number): void {
    this.totalsService.list(page, this.sortConfig.field, this.sortConfig.direction)
      .subscribe(
        response => {
          this.totals = [];
          const totals = response._embedded ? response._embedded.resources : [];
          for (const total of totals) {
            this.totals.push(total);
          }
          this.page = response.page;
        }
      );
  }

  numberWithCommas(x): string {
    return x.toString().replace(",", ".");
  }

  getPages(): string[] {
    const possibleChoices: string[] = ['1'];

    if (this.page.totalPages > 1) {
      if (this.page.totalPages > 5) {
        if (this.page.number > 1 && this.page.number < this.page.totalPages - 2) {
          const currentPage = this.page.number + 1;
          possibleChoices.push('...');
          possibleChoices.push(this.page.number.toString());
          possibleChoices.push(currentPage.toString());
          possibleChoices.push((currentPage + 1).toString());
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else if (this.page.number < this.page.totalPages - 2) {
          possibleChoices.push('2');
          possibleChoices.push('3');
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else {
          possibleChoices.push('2');
          possibleChoices.push('...');
          possibleChoices.push((this.page.totalPages - 2).toString());
          possibleChoices.push((this.page.totalPages - 1).toString());
          possibleChoices.push(this.page.totalPages.toString());
        }
      } else {
        for (let i = 1; i < this.page.totalPages; i++) {
          possibleChoices.push((i + 1).toString());
        }
      }
    }

    return possibleChoices;
  }

  changePage(toPage: string): void {
    if (toPage !== '...') {
      this.getData(+toPage - 1);
    }
  }

}
