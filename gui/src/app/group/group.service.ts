///<reference path="../model/uif-http.ts"/>
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Group} from './group';
import {Response, URLSearchParams} from '@angular/http';
import {environment} from '../../environments/environment';
import {RestResponse} from '../model/rest-response';
import {UifHttp} from '../model/uif-http';
import 'rxjs/add/operator/map';

const SYSTEM_PREFIX = environment.BACKEND;
const PARAM = 'groups';

@Injectable()
export class GroupService {

  constructor(private http: UifHttp) {
  }

  list(query:string,page = 0, sort = 'created', direction = 'desc'): Observable<RestResponse<Group>> {
    const listUrl = SYSTEM_PREFIX + PARAM+ "/search/findAllLike";;
    const params = new URLSearchParams();
    params.set('page', page.toString());
    params.set('sort', sort + ',' + direction);
    if(query) {
      params.set('search', query.replace('.', ','));
    }
    return this.http.get(listUrl, {search: params}).map(response => response.json() as RestResponse<Group>);
  }

  edit(group: Group): Observable<Response> {
    const updateUrl = SYSTEM_PREFIX + PARAM + '/' + group.sysId;
    return this.http.put(updateUrl, group);
  }

  add(group: Group): Observable<Response> {
    const addUrl = SYSTEM_PREFIX + PARAM;
    return this.http.post(addUrl, group);
  }

  delete(group: Group): Observable<Response> {
    const deleteUrl = SYSTEM_PREFIX + PARAM + '/' + group.sysId;
    return this.http.delete(deleteUrl);
  }

}
