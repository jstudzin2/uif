package pl.javalia.visualiser.model;

import java.util.Date;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place description here.
 *
 * @author Q1JS@nykredit.dk
 */
@Entity
@Table(name = "SG3_536_STATUS_WORKAREA_V")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GenexStatus {

    @Id
    @Column(name = "PTIME")
    private String date;

    @Column(name = "PDESC")
    private String description;

    @Column(name = "PSTATUS")
    private String system;
}
