export class Page {
    size: number;
totalElements: number;
totalPages: number;
number: number;


getPages(): string[] {
        const possibleChoices: string[] = ['1'];

        if (this.totalPages > 1) {
            if (this.totalPages > 5) {
                if (this.number > 1 && this.number < this.totalPages - 2) {
                    const currentPage = this.number + 1;
                    possibleChoices.push('...');
                    possibleChoices.push(this.number.toString());
                    possibleChoices.push(currentPage.toString());
                    possibleChoices.push((currentPage + 1).toString());
                    possibleChoices.push('...');
                    possibleChoices.push(this.totalPages.toString());
                } else if (this.number < this.totalPages - 2) {
                    possibleChoices.push('2');
                    possibleChoices.push('3');
                    possibleChoices.push('...');
                    possibleChoices.push(this.totalPages.toString());
                } else {
                    possibleChoices.push('2');
                    possibleChoices.push('...');
                    possibleChoices.push((this.totalPages - 2).toString());
                    possibleChoices.push((this.totalPages - 1).toString());
                    possibleChoices.push(this.totalPages.toString());
                }
            } else {
                for (let i = 1; i < this.totalPages; i++) {
                    possibleChoices.push((i + 1).toString());
                }
            }
        }

        return possibleChoices;
    }
}