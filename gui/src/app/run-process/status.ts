export class Status {
  processId: number;
  running: boolean;
  name: string;
}
