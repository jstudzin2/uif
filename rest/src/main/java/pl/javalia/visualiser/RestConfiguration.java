package pl.javalia.visualiser;

import org.apache.catalina.connector.Connector;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * Created by Kuba&Sylwia on 09.02.18.
 */
@Configuration
public class RestConfiguration {


    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("OPTIONS");
        config.addAllowedMethod("GET");
        config.addAllowedMethod("POST");
        config.addAllowedMethod("PUT");
        config.addAllowedMethod("DELETE");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }

//    @Bean
//    public EmbeddedServletContainerFactory servletContainer() {
//
//        TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();
//        Connector ajpConnector = new Connector("AJP/1.3");
//        ajpConnector.setProtocol("AJP/1.3");
//        ajpConnector.setPort(8009);
//        ajpConnector.setSecure(false);
//        ajpConnector.setRedirectPort(8443);
//        ajpConnector.setEnableLookups(false);
//        ajpConnector.setAllowTrace(false);
//        ajpConnector.setScheme("http");
//        tomcat.addAdditionalTomcatConnectors(ajpConnector);
//
//        return tomcat;
//    }
}
