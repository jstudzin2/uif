package pl.javalia.visualiser.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * Place description here.
 *
 * @author Q1JS@nykredit.dk
 */
@Entity
@Table(name = "SG3_536_FILE_STATS_WORKAREA_V")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Total {

    @Id
    @Column(name = "SOURCEID")
    private String source;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @Column(name = "RECORDS")
    private Long records;
}
