import {Component, OnInit} from '@angular/core';
import {Account} from './account';
import {AccountService} from './account.service';
import {Page} from '../model/page';
import {SortConfig} from '../model/sort-config';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  public accounts: Account[] = [];
  editCopy: Account;
  public page: Page = null;
  public sortConfig = new SortConfig('syssec', this);
  public query:string;
  constructor(private accountService: AccountService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData(page?: number): void {
    this.accountService.list(this.query,page, this.sortConfig.field, this.sortConfig.direction)
      .subscribe(
        response => {
          this.accounts = [];
          const accounts = response._embedded ? response._embedded.resources : [];
          for (const account of accounts) {
            this.accounts.push(account);
          }
          this.page = response.page;
        }
      );
  }

  edit(account: Account) {
    this.editCopy = new Account();
    this.editCopy.acct = account.acct;
    this.editCopy.id = account.id ;
    this.editCopy.ldesc = account.ldesc;
    this.editCopy.ldesc1 = account.ldesc1;
    this.editCopy.leg = account.leg;
    this.editCopy.sdesc = account.sdesc;
    this.editCopy.syssec = account.syssec;
    this.editCopy.tt = account.tt;
    account.edition = true;
  }

  delete(account: Account) {
    this.accountService.delete(account).subscribe(() => this.getData());
  }


  cancel(account: Account) {
    if( account.new){
        this.accounts = this.accounts.filter( (acc) => !acc.new);
    }else{
      account.acct= this.editCopy.acct;
      account.id = this.editCopy.id;
      account.ldesc= this.editCopy.ldesc;
      account.ldesc1= this.editCopy.ldesc1;
      account.leg= this.editCopy.leg;
      account.sdesc= this.editCopy.sdesc;
      account.syssec= this.editCopy.syssec;
      account.tt= this.editCopy.tt;
        account.edition=false
    }
  }

  save(account: Account) {
    if (account.new) {
      this.accountService.add(account).subscribe(() => this.getData());
    } else {
      this.accountService.edit(account).subscribe(() => this.getData());
    }
  }

  add() {
    const account = new Account;
    account.edition = true;
    account.new = true;
    this.accounts.push(account);
  }

  changePage(toPage: string): void {
    if (toPage !== '...') {
      this.getData(+toPage - 1);
    }
  }

  getPages(): string[] {
    const possibleChoices: string[] = ['1'];

    if (this.page.totalPages > 1) {
      if (this.page.totalPages > 5) {
        if (this.page.number > 1 && this.page.number < this.page.totalPages - 2) {
          const currentPage = this.page.number + 1;
          possibleChoices.push('...');
          possibleChoices.push(this.page.number.toString());
          possibleChoices.push(currentPage.toString());
          possibleChoices.push((currentPage + 1).toString());
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else if (this.page.number < this.page.totalPages - 2) {
          possibleChoices.push('2');
          possibleChoices.push('3');
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else {
          possibleChoices.push('2');
          possibleChoices.push('...');
          possibleChoices.push((this.page.totalPages - 2).toString());
          possibleChoices.push((this.page.totalPages - 1).toString());
          possibleChoices.push(this.page.totalPages.toString());
        }
      } else {
        for (let i = 1; i < this.page.totalPages; i++) {
          possibleChoices.push((i + 1).toString());
        }
      }
    }

    return possibleChoices;
  }

  downloadCSV() {
    var nameOfFileToDownload = "Accounts.csv";
    this.accountService.csv().subscribe(
      success => {
        var blob = new Blob([success._body], { type: 'text/csv' });

        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(blob, nameOfFileToDownload);
        } else {
          var a = document.createElement('a');
          a.href = URL.createObjectURL(blob);
          a.download = nameOfFileToDownload;
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
        }
      },
      err => {
        alert("Server error while downloading file.");
      }
    );
  }

}
