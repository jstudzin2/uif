package pl.javalia.visualiser.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Place description here.
 *
 * @author Q1JS@nykredit.dk
 */
@Entity
@Table(name = "SG3_536_TTYPE_MAP")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GroupMapping {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="SG3_536_ID")
    @SequenceGenerator(name="SG3_536_ID", sequenceName="SG3_536_ID")
    @Column(name = "SYSID")
    private Long sysId;

    @Column(name = "TTYPE")
    @Length(max = 50)
    private String ttype;

    @Length(max = 15)
    @Column(name = "TT")
    private String tt;

    @Length(max = 10)
    @Column(name = "SOURCE")
    private String source;
}
