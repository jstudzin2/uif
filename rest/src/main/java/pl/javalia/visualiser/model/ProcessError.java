package pl.javalia.visualiser.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

/**
 * Place description here.
 *
 * @author Q1JS@nykredit.dk
 */
@Entity
@Table(name = "SG3_536_ERROR_TRANS_WORKAREA_V")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@IdClass(ErrorPK.class)
public class ProcessError {

    @Id
    @Column(name = "SOURCEID")
    private String source;

    @Id
    @Column(name = "PDESC")
    private String description;

    @Id
    @Column(name = "OTHER")
    private String moreInfo;
}
