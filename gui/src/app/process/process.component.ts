import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {GenexStatusComponent} from '../genex-status/genex-status.component';
import {TotalsComponent} from '../totals/totals.component';
import {ProcessValidationComponent} from '../process-validation/process-validation.component';
import {ProcessErrorComponent} from '../process-error/process-error.component';
import {RunProcessComponent} from '../run-process/run-process.component';

@Component({
  selector: 'app-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.css']
})
export class ProcessComponent implements OnInit, OnDestroy {

  @ViewChild('genexStatus') genexStatusComponent: GenexStatusComponent;
  @ViewChild('processError') processErrorComponent: ProcessErrorComponent;
  @ViewChild('processValidation') processValidationComponent: ProcessValidationComponent;
  @ViewChild('totals') totalsComponent: TotalsComponent;
  @ViewChild('run') runProcessComponent: RunProcessComponent;
  @ViewChild('runSA') runProcessSAComponent: RunProcessComponent;
  interval: any;

  constructor() {
  }

  ngOnInit() {
    this.interval = setInterval(() => {
      this.genexStatusComponent.getData(this.genexStatusComponent.page.number);
      this.processErrorComponent.getData(this.processErrorComponent.page.number);
      this.processValidationComponent.getData(this.processValidationComponent.page.number);
      this.totalsComponent.getData(this.totalsComponent.page.number);
      this.runProcessComponent.refresh();
      this.runProcessSAComponent.refresh();
    }, 5000);
  }

  ngOnDestroy() {
    clearInterval(this.interval);
  }

}
