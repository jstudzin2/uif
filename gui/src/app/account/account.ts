import {Links} from '../model/links';

export class Account {
    id: number
    syssec: string;
    sdesc: string;
    tt: string;
    ldesc: string;
    acct: string;
    leg: string;
    ldesc1: string;
    edition = false;
    new = false;
    _links: Links;
}
