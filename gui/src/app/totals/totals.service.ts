import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {URLSearchParams} from '@angular/http';
import {environment} from '../../environments/environment';
import {RestResponse} from '../model/rest-response';
import {UifHttp} from '../model/uif-http';
import 'rxjs/add/operator/map';
import {Totals} from './totals';

const SYSTEM_PREFIX = environment.BACKEND;
const PARAM = 'totals';

@Injectable()
export class TotalsService {

  constructor(private http: UifHttp) {
  }

  list(page = 0, sort = 'created', direction = 'desc'): Observable<RestResponse<Totals>> {
    const listUrl = SYSTEM_PREFIX + PARAM;
    const params = new URLSearchParams();
    params.set('page', page.toString());
    params.set('sort', sort + ',' + direction);
    return this.http.get(listUrl, {search: params}).map(response => response.json() as RestResponse<Totals>);
  }

}
