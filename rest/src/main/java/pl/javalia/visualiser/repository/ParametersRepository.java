package pl.javalia.visualiser.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import pl.javalia.visualiser.model.GroupMapping;
import pl.javalia.visualiser.model.Parameter;

/**
 * Place description here.
 *
 */

@RepositoryRestResource(collectionResourceRel = "resources", path = "parameters")
public interface ParametersRepository extends PagingAndSortingRepository<Parameter, Long> {

    @RestResource(exported = true)
    @Query("select parameter from Parameter parameter where " +
            "LOWER(parameter.description) like LOWER(:search||'%')")
    Page<Parameter> findAllLike(@Param("search") String search, Pageable pageable);

}