package pl.javalia.visualiser.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Place description here.
 *
 * @author Q1JS@nykredit.dk
 */
@Entity
@Table(name = "SG3_536_BROKER_NOTES_PROCESS")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReportTransactionBroker {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="SG3_536_ID")
    @SequenceGenerator(name="SG3_536_ID", sequenceName="SG3_536_ID")
    @Column(name = "SYSID")
    private Long id;

    @Column(name = "RECON_ID")
    private Long reconId;

    @Column(name = "MANUAL_RECON")
    private Long manualId;

    @Length(max = 10)
    @Column(name = "SYSSEC")
    private String security;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @Column(name = "PROCEEDS")
    private BigDecimal proceeds;

    @Column(name = "TOTAL_EXP")
    private BigDecimal totalExp;

    @Column(name = "BR_AMOUNT")
    private BigDecimal brAmount;

    @Column(name = "WORK_AMOUNT")
    private BigDecimal workAmount;

    @Length(max = 30)
    @Column(name = "GROUPID")
    private String groupId;

    @Length(max = 1)
    @Column(name = "SKIP_FLAG")
    private String skip;

    @Length(max = 2)
    @Column(name = "RECON_FLAG")
    private String reconFlag;

    @Length(max = 2)
    @Column(name = "GNX_FLAG")
    private String gnxFlag;


    @Length(max = 30)
    @Column(name = "ORG_TTYPE")
    private String tt;

    @Length(max = 2)
    @Column(name = "MAP_TTYPE")
    private String mtt;

    @Column(name = "DATE_REC")
    private Date date;

    @Length(max = 200)
    @Column(name = "DESCRIPTION")
    private String description;

    @Length(max = 25)
    @Column(name = "PORTFOLIO1")
    private String portfolio;
}
