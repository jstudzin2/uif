///<reference path="../model/uif-http.ts"/>
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Account} from './account';
import {Response, URLSearchParams} from '@angular/http';
import {environment} from '../../environments/environment';
import {RestResponse} from '../model/rest-response';
import {UifHttp} from '../model/uif-http';
import 'rxjs/add/operator/map';

const SYSTEM_PREFIX = environment.BACKEND;
const ACCOUNT = 'accounts';

@Injectable()
export class AccountService {

  constructor(private http: UifHttp) {
  }

  list(query:string, page = 0, sort = 'created', direction = 'desc'): Observable<RestResponse<Account>> {
    const listUrl = SYSTEM_PREFIX + ACCOUNT+ "/search/findAllLike";;
    const params = new URLSearchParams();
    params.set('page', page.toString());
    params.set('sort', sort + ',' + direction);
    if(query) {
      params.set('search', query.replace('.', ','));
    }
    return this.http.get(listUrl, {search: params}).map(response => response.json() as RestResponse<Account>);
  }

  edit(account: Account): Observable<Response> {
    const updateUrl = SYSTEM_PREFIX + ACCOUNT + '/' + account.id;
    return this.http.put(updateUrl, account);
  }

  add(account: Account): Observable<Response> {
    const addUrl = SYSTEM_PREFIX + ACCOUNT;
    return this.http.post(addUrl, account);
  }

  delete(account: Account): Observable<Response> {
    const deleteUrl = SYSTEM_PREFIX + ACCOUNT + '/' + account.id;
    return this.http.delete(deleteUrl);
  }

  csv():any{
    return this.http.get(SYSTEM_PREFIX+"csv/account");
  }

}
