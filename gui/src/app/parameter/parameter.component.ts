import {Component, OnInit} from '@angular/core';
import {Parameter} from './parameter';
import {ParameterService} from './parameter.service';
import {Page} from '../model/page';
import {SortConfig} from '../model/sort-config';

@Component({
  selector: 'app-parameter',
  templateUrl: './parameter.component.html',
  styleUrls: ['./parameter.component.css']
})
export class ParameterComponent implements OnInit {

  public parameters: Parameter[] = [];
  editCopy: Parameter;

  public page: Page = null;
  public sortConfig = new SortConfig('paramId', this);
  public query:string;
  constructor(private parameterService: ParameterService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData(page?: number): void {
    this.parameterService.list(this.query,page, this.sortConfig.field, this.sortConfig.direction)
      .subscribe(
        response => {
          this.parameters = [];
          const parameters = response._embedded ? response._embedded.resources : [];
          for (const parameter of parameters) {
            this.parameters.push(parameter);
          }
          this.page = response.page;
        }
      );
  }

  edit(parameter: Parameter) {
    this.editCopy = new Parameter();
    this.editCopy.value = parameter.value;
    this.editCopy.description = parameter.description;
    parameter.edition = true;
  }

  cancel(parameter: Parameter) {
    if( parameter.new){
        this.parameters = this.parameters.filter( (param) => !param.new);
    }else{
      parameter.value = this.editCopy.value;
      parameter.description = this.editCopy.description;
        parameter.edition=false
    }
  }

  delete(parameter: Parameter) {
    this.parameterService.delete(parameter).subscribe(() => this.getData());
  }

  save(parameter: Parameter) {
    if (parameter.new) {
      this.parameterService.add(parameter).subscribe(() => this.getData());
    } else {
      this.parameterService.edit(parameter).subscribe(() => this.getData());
    }
  }

  add() {
    const parameter = new Parameter;
    parameter.edition = true;
    parameter.new = true;
    this.parameters.push(parameter);
  }

  changePage(toPage: string): void {
    if (toPage !== '...') {
      this.getData(+toPage - 1);
    }
  }

  getPages(): string[] {
    const possibleChoices: string[] = ['1'];

    if (this.page.totalPages > 1) {
      if (this.page.totalPages > 5) {
        if (this.page.number > 1 && this.page.number < this.page.totalPages - 2) {
          const currentPage = this.page.number + 1;
          possibleChoices.push('...');
          possibleChoices.push(this.page.number.toString());
          possibleChoices.push(currentPage.toString());
          possibleChoices.push((currentPage + 1).toString());
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else if (this.page.number < this.page.totalPages - 2) {
          possibleChoices.push('2');
          possibleChoices.push('3');
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else {
          possibleChoices.push('2');
          possibleChoices.push('...');
          possibleChoices.push((this.page.totalPages - 2).toString());
          possibleChoices.push((this.page.totalPages - 1).toString());
          possibleChoices.push(this.page.totalPages.toString());
        }
      } else {
        for (let i = 1; i < this.page.totalPages; i++) {
          possibleChoices.push((i + 1).toString());
        }
      }
    }

    return possibleChoices;
  }
}
