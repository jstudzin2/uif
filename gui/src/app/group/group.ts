import {Links} from '../model/links';

export class Group {
    sysId: number;
    ttype: string;
    tt: string;
    source: string;
    edition = false;
    new = false;
    _links: Links;
}
