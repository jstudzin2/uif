import {Component, OnInit} from '@angular/core';
import {CookieService} from "ngx-cookie";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'app';

  showMenu: boolean;

  constructor(private _cookieService: CookieService, private router: Router) {
  }

  ngOnInit(): void {
    this.showMenu = true;
    const cookie = this._cookieService.get("PAMAUTH");
    // if (!cookie) {
    //   this.showMenu = false;
    //   this.router.navigate(['/unauthorized']);
    // }
  }
}
