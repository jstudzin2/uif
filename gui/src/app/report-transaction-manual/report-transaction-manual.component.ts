import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ReportTransation} from '../report-transaction/reportTransation';

import {Page} from '../model/page';
import {SortConfig} from '../model/sort-config';
import {ReportTransationManualService} from './reportTransactionManual.service';
import {StatusService} from "../run-process/status.service";
import {Modal} from "ngx-modal";

@Component({
  selector: 'app-report-transaction-manual',
  templateUrl: './report-transaction-manual.component.html',
  styleUrls: ['./report-transaction-manual.component.css']
})
export class ReportTransactionManualComponent implements OnInit {

  public reportTransations: ReportTransation[] = [];
  @ViewChild('myModal') modal: Modal;
  public name: string;

  public recon: boolean = false;
  editCopy: ReportTransation;
  public query: string;
  public page: Page = null;
  public sortConfig = new SortConfig('id', this);
  interval: any;
  public report: number;

  public running: boolean = false;

  private processId: number;

  constructor(private reportTransationService: ReportTransationManualService, private statusService: StatusService) {
  }

  ngOnInit() {
    this.report = 1;
    this.name = "Transaction Report";
    this.recon=true;
    this.getData();
  }

  numberWithCommas(x): string {
    if(x) {
      return x.toString().replace(",", ".");
    }
    return;
  }

  getData(page?: number): void {
    if (this.report) {
      this.reportTransationService.list(this.query, this.recon, page, this.sortConfig.field, this.sortConfig.direction, this.report)
        .subscribe(
          response => {
            this.reportTransations = [];
            const reportTransations = response._embedded ? response._embedded.resources : [];
            for (const reportTransation of reportTransations) {
              this.reportTransations.push(reportTransation);
            }
            this.page = response.page;
          }
        );
    }
  }

  filterRecon(e) {
    this.recon = e.target.checked;
    this.getData();
  }

  edit(reportTransation: ReportTransation) {
    this.editCopy = new ReportTransation();
    this.editCopy.gnxFlag = reportTransation.gnxFlag;
    this.editCopy.manualId= reportTransation.manualId;
    this.editCopy.security= reportTransation.security;
    this.editCopy.groupId= reportTransation.groupId;
    this.editCopy.tt= reportTransation.tt;
    this.editCopy.mtt= reportTransation.mtt;
    this.editCopy.portfolio= reportTransation.portfolio;
    reportTransation.edition = true;
  }

  delete(reportTransation: ReportTransation) {
    reportTransation.gnxFlag = "H";
    this.save(reportTransation);
  }

  save(reportTransation: ReportTransation) {
    reportTransation.skip = "M";
    reportTransation.reconFlag = "M0";
    if (reportTransation.new) {
      this.reportTransationService.add(reportTransation, this.report).subscribe(() => this.getData(this.page.number));
    } else {
      this.reportTransationService.edit(reportTransation, this.report).subscribe(() => this.getData(this.page.number));
    }
  }

  cancel(reportTransation: ReportTransation) {
    if (reportTransation.new) {
      this.reportTransations = this.reportTransations.filter((param) => !param.new);
    } else {
      reportTransation.gnxFlag = this.editCopy.gnxFlag;
      reportTransation.manualId = this.editCopy.manualId;
      reportTransation.security = this.editCopy.security;
      reportTransation.groupId  = this.editCopy.groupId;
      reportTransation.tt = this.editCopy.tt;
      reportTransation.mtt = this.editCopy.mtt;
      reportTransation.portfolio = this.editCopy.portfolio;
      reportTransation.edition = false
    }
  }

  add() {
    const reportTransation = new ReportTransation;
    reportTransation.edition = true;
    reportTransation.new = true;
    this.reportTransations.push(reportTransation);
  }

  load(file: number) {
    if (file == 1) {
      this.name = "Transaction report";
    }
    if (file == 2) {
      this.name = "Bank statements";
    }
    if (file == 3) {
      this.name = "Expenditures transactions";
    }
    this.report = file;
    this.getData();
  }

  collspan(){
    if (this.report == 1) {
      return 18;
    }
    if (this.report == 2) {
      return 15;
    }
    if (this.report == 3) {
      return 15;
    }
  }

  changePage(toPage: string): void {
    if (toPage !== '...') {
      this.getData(+toPage - 1);
    }
  }


  getPages(): string[] {
    const possibleChoices: string[] = ['1'];

    if (this.page.totalPages > 1) {
      if (this.page.totalPages > 5) {
        if (this.page.number > 1 && this.page.number < this.page.totalPages - 2) {
          const currentPage = this.page.number + 1;
          possibleChoices.push('...');
          possibleChoices.push(this.page.number.toString());
          possibleChoices.push(currentPage.toString());
          possibleChoices.push((currentPage + 1).toString());
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else if (this.page.number < this.page.totalPages - 2) {
          possibleChoices.push('2');
          possibleChoices.push('3');
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else {
          possibleChoices.push('2');
          possibleChoices.push('...');
          possibleChoices.push((this.page.totalPages - 2).toString());
          possibleChoices.push((this.page.totalPages - 1).toString());
          possibleChoices.push(this.page.totalPages.toString());
        }
      } else {
        for (let i = 1; i < this.page.totalPages; i++) {
          possibleChoices.push((i + 1).toString());
        }
      }
    }

    return possibleChoices;
  }

  manual() {
    this.processId = 4;
  }

  allAndManual() {
    this.processId = 5
  }

  cleanAuto() {
    this.processId = 7
  }


  run() {
    this.running = true;
    this.statusService.runById(this.processId).subscribe(
      response=>{
        this.getData();
        this.running=false;
        this.modal.close();
      }
    );
  }
}
