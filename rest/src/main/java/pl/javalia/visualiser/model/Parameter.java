package pl.javalia.visualiser.model;

import java.util.Date;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place description here.
 *
 * @author Q1JS@nykredit.dk
 */
@Entity
@Table(name = "SG3_536_APP_PARAM_WORKAREA")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Parameter {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="SG3_536_ID")
    @SequenceGenerator(name="SG3_536_ID", sequenceName="SG3_536_ID")
    @Column(name = "SYSID")
    private Long paramId;

    @Column(name = "DESC_TEXT")
    private String description;

    @Column(name = "VALUE_ID")
    private String value;
}
