import {Links} from '../model/links';

export class Genex {
  id:number
  account: string;
  amount: number;
  description: string;
  date: Date;
  edition = false;
  new = false;
  _links: Links;
}
