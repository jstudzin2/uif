package pl.javalia.visualiser.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import pl.javalia.visualiser.model.Status;
import pl.javalia.visualiser.repository.StatusRepository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kuba&Sylwia on 11.02.18.
 */
@Service
public class RunService {
    private static final Logger log = Logger.getLogger(RunService.class);
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private StatusRepository statusRepository;


    @Autowired
    public void init(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static Map<Integer, String> procMapping;

    static {
        procMapping = new HashMap();
        procMapping.put(0, "SG3_536_CLEANUP_PROCESS()");
        procMapping.put(1, "SG3_536_AUTO_RECON_PROCESS()");
        procMapping.put(2, "SG3_536_GEN_GENEX_FILE('GRO')");
        procMapping.put(3, "SG3_536_GEN_GENEX_FILE('GPR')");
        procMapping.put(4, "SG3_536_MANUAL_RECON_PROCESS('MANUAL')");
        procMapping.put(5, "SG3_536_MANUAL_RECON_PROCESS('AUTO')");
        procMapping.put(6, "SG3_536_GEN_GENEX_FILE('WORKAREA')");
        procMapping.put(7, "SG3_536_MANUAL_RECON_PROCESS('CLEAN')");
        procMapping.put(8, "SG3_536_SET_POST_PERIOD('XXX')");
        procMapping.put(9, "SG3_536_LOAD_REVERSALS()");
        procMapping.put(10, "SG3_536_EXCEPTIONS_UPDATE(XXX)");
        procMapping.put(11, "SG3_536_LOAD_ACCRUALS()");
        procMapping.put(12, "SG3_536_UPDATE_GENEX_TABLE()");
        procMapping.put(90, "SG3_536_LOAD_EXPEDITURES()");
    }

    public String run(int processNo) {
        Status status = statusRepository.findOne(Long.valueOf(processNo));
        String procedureToCall = procMapping.get(processNo);
        if (status != null) {
            if (status.getProcedure() != null) {
                procedureToCall = status.getProcedure();
            }
        }
        if (procedureToCall == null) {
            log.info("Procedure for process " + processNo + " NOT FOUND");
            return procedureToCall;
        }
        log.info("Executing procedure " + procedureToCall);
        this.jdbcTemplate.update("call " + procedureToCall);
        return procedureToCall;
    }

    public String runWithArgument(int processNo, String argument) {
        String procedureToCall = procMapping.get(processNo);
        log.info("Executing procedure " + procMapping.get(processNo));
        String replacedCall = procedureToCall.replace("XXX", argument);
        log.info("Executing procedure with parameter " + replacedCall);
        this.jdbcTemplate.update("call " + replacedCall);
        return procedureToCall;
    }

    public String runWithArguments(int processNo, String... arguments) {
        StringBuilder sb = new StringBuilder("");
        for (String argument : arguments) {
            sb.append(",").append("'").append(argument).append("'");
        }
        return runWithArgument(processNo, sb.substring(1).toString());
    }
}
