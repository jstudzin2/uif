///<reference path="../model/uif-http.ts"/>
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Parameter} from './parameter';
import {Response, URLSearchParams} from '@angular/http';
import {environment} from '../../environments/environment';
import {RestResponse} from '../model/rest-response';
import {UifHttp} from '../model/uif-http';
import 'rxjs/add/operator/map';

const SYSTEM_PREFIX = environment.BACKEND;
const PARAM = 'parameters';

@Injectable()
export class ParameterService {

  constructor(private http: UifHttp) {
  }

  list(query:string,page = 0, sort = 'created', direction = 'desc'): Observable<RestResponse<Parameter>> {
    const listUrl = SYSTEM_PREFIX + PARAM+ "/search/findAllLike";
    const params = new URLSearchParams();
    params.set('page', page.toString());
    params.set('sort', sort + ',' + direction);
    if(query) {
      params.set('search', query.replace('.', ','));
    }
    return this.http.get(listUrl, {search: params}).map(response => response.json() as RestResponse<Parameter>);
  }

  edit(parameter: Parameter): Observable<Response> {
    const updateUrl = SYSTEM_PREFIX + PARAM + '/' + parameter.paramId;
    return this.http.put(updateUrl, parameter);
  }

  add(parameter: Parameter): Observable<Response> {
    const addUrl = SYSTEM_PREFIX + PARAM;
    return this.http.post(addUrl, parameter);
  }

  delete(parameter: Parameter): Observable<Response> {
    const deleteUrl = SYSTEM_PREFIX + PARAM + '/' + parameter.paramId;
    return this.http.delete(deleteUrl);
  }

}
