import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportTransactionManualComponent } from './report-transaction-manual.component';

describe('ReportTransactionManualComponent', () => {
  let component: ReportTransactionManualComponent;
  let fixture: ComponentFixture<ReportTransactionManualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportTransactionManualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportTransactionManualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
