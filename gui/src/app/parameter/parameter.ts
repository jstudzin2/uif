import {Links} from '../model/links';

export class Parameter {
  paramId: number;
  description: string;
  value: string;
  edition = false;
  new = false;
  _links: Links;
}
