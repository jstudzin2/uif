package pl.javalia.visualiser.model;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Place description here.
 *
 * @author Q1JS@nykredit.dk
 */
@Entity
@Table(name = "SG3_536_VALIDATION_WORKAREA_V")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProcessValidation {

    @Id
    @Column(name = "SOURCEID")
    private String source;

    @Column(name = "NAMEID")
    private String name;

    @Column(name = "STATUS")
    private String status;
}
