export class ProcessError {
  source: string;
  description: string;
  moreInfo: string;
}
