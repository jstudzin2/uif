import {Component, OnInit} from '@angular/core';
import {Group} from '../group/group';
import {GroupService} from './group.service';
import {Page} from '../model/page';
import {SortConfig} from '../model/sort-config';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {
  public query:string;
  public groups: Group[] = [];
  public page: Page = null;
  public sortConfig = new SortConfig('ttype', this);

  constructor(private groupService: GroupService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData(page?: number): void {
    this.groupService.list(this.query,page, this.sortConfig.field, this.sortConfig.direction)
      .subscribe(
        response => {
          this.groups = [];
          const groups = response._embedded ? response._embedded.resources : [];
          for (const group of groups) {
            this.groups.push(group);
          }
          this.page = response.page;
        }
      );
  }

  edit(group: Group) {
    group.edition = true;
  }

  cancel(group: Group) {
    if( group.new){
        this.groups = this.groups.filter( (gr) => !gr.new);
    }else{
        group.edition=false;
    }
  }

  delete(group: Group) {
    this.groupService.delete(group).subscribe(() => this.getData());
  }

  save(group: Group) {
    if (group.new) {
      this.groupService.add(group).subscribe(() => this.getData());
    } else {
      this.groupService.edit(group).subscribe(() => this.getData());
    }
  }

  add() {
    const group = new Group;
    group.edition = true;
    group.new = true;
    this.groups.push(group);
  }

  changePage(toPage: string): void {
    if (toPage !== '...') {
      this.getData(+toPage - 1);
    }
  }

  getPages(): string[] {
    const possibleChoices: string[] = ['1'];

    if (this.page.totalPages > 1) {
      if (this.page.totalPages > 5) {
        if (this.page.number > 1 && this.page.number < this.page.totalPages - 2) {
          const currentPage = this.page.number + 1;
          possibleChoices.push('...');
          possibleChoices.push(this.page.number.toString());
          possibleChoices.push(currentPage.toString());
          possibleChoices.push((currentPage + 1).toString());
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else if (this.page.number < this.page.totalPages - 2) {
          possibleChoices.push('2');
          possibleChoices.push('3');
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else {
          possibleChoices.push('2');
          possibleChoices.push('...');
          possibleChoices.push((this.page.totalPages - 2).toString());
          possibleChoices.push((this.page.totalPages - 1).toString());
          possibleChoices.push(this.page.totalPages.toString());
        }
      } else {
        for (let i = 1; i < this.page.totalPages; i++) {
          possibleChoices.push((i + 1).toString());
        }
      }
    }

    return possibleChoices;
  }
}
