package pl.javalia.visualiser.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import pl.javalia.visualiser.model.GenexFile;

/**
 * Place description here.
 *
 */

@RepositoryRestResource(collectionResourceRel = "resources", path = "files")
public interface GenexFileRepository extends PagingAndSortingRepository<GenexFile, Long> {

    @RestResource(exported = true)
    @Query("select genexFile from GenexFile genexFile where " +
            "LOWER(genexFile.description) like LOWER('%'||:search||'%') or LOWER(genexFile.account) like LOWER(:search||'%')" +
            " or LOWER(TO_CHAR(genexFile.date, 'DDMMYYYY')) like LOWER(:search||'%')  or LOWER(genexFile.amount) like LOWER(:search||'%')")
    Page<GenexFile> findAllLike(@Param("search") String search, Pageable pageable);
}