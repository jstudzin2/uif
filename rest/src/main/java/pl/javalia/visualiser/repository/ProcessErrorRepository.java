package pl.javalia.visualiser.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import pl.javalia.visualiser.model.GenexStatus;
import pl.javalia.visualiser.model.ProcessError;

/**
 * Place description here.
 *
 * @author Q1JS@nykredit.dk
 */

@RepositoryRestResource(collectionResourceRel = "resources", path = "errors")
public interface ProcessErrorRepository extends PagingAndSortingRepository<ProcessError, Long> {

    @RestResource(exported = true)
    @Query("select processError from ProcessError processError where " +
            "LOWER(processError.source) like LOWER(:search||'%') or LOWER(processError.description) like LOWER(:search||'%')" +
            " or LOWER(processError.moreInfo) like LOWER(:search||'%')")
    Page<ProcessError> findAllLike(@Param("search") String search, Pageable pageable);
}