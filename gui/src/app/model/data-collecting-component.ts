export interface DataCollectingComponent {
    getData(page?: number): void;
}
