package pl.javalia.visualiser.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Place description here.
 *
 * @author Q1JS@nykredit.dk
 */
@Entity
@Table(name = "SG3_536_STATUS_UI")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Status {

    @Id
    @Column(name = "PROCESS_ID")
    private Long processId;

    @Column(name = "IS_RUNNING")
    private Boolean running;

    @Column(name = "ORDER_ON_VIEW")
    private int onView;

    @Column(name = "NAME")
    private String name;

    @Column(name = "STAND_ALONE")
    private boolean standAlone;

    @Column(name = "PROC_NAME")
    private String procedure;

}
