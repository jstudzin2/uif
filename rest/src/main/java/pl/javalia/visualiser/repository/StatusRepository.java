package pl.javalia.visualiser.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import pl.javalia.visualiser.model.Status;

import java.util.List;

/**
 * Place description here.
 *
 * @author Q1JS@nykredit.dk
 */

@RepositoryRestResource(collectionResourceRel = "resources", path = "status")
public interface StatusRepository extends PagingAndSortingRepository<Status, Long> {

    @RestResource(exported = true)
    @Query("select stat from Status stat where stat.standAlone=:standAlone ORDER BY stat.onView asc")
    List<Status> findAll(@Param("standAlone") boolean standAlone);
}