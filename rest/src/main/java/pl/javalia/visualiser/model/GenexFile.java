package pl.javalia.visualiser.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Place description here.
 *
 * @author Q1JS@nykredit.dk
 */
@Entity
@Table(name = "SG3_536_GENEX_FILE_WORKAREA")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GenexFile {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator="SG3_536_ID")
    @SequenceGenerator(name="SG3_536_ID", sequenceName="SG3_536_ID")
    @Column(name = "SYSID")
    private Long id;

    @Column(name = "ACCOUNTID")
    private String account;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @Column(name = "DESC_TEXT")
    private String description;

    @Column(name = "DATE_GENEX")
    private Date date;
}
