import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenexComponent } from './genex.component';

describe('GenexComponent', () => {
  let component: GenexComponent;
  let fixture: ComponentFixture<GenexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
