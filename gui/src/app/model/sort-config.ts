import {DataCollectingComponent} from './data-collecting-component';

export class SortConfig {

    direction = 'asc';

    constructor(public field: string, private collectingComponent: DataCollectingComponent) {

    }

    sortProperty() {
        return this.field;
    }

    changeSortField(toField: string): void {
        if (this.field !== toField) {
            this.field = toField;
            this.direction = 'asc';
        } else {
            if ('asc' === this.direction) {
                this.direction = 'desc';
            } else {
                this.direction = 'asc';
            }
        }
        this.collectingComponent.getData(0);
    }

    changeSortFieldToDirection(toField: string, direction: string): void {
        if (this.field === toField && this.direction === direction) {
            return;
        }
        this.field = toField;
        this.direction = direction;
        this.collectingComponent.getData(0);
    }

    sortStyle(field: string): string {
        if (field === this.field) {
            if (this.direction === 'asc') {
                return 'sortable-asc';
            } else {
                return 'sortable-desc';
            }
        } else {
            return 'sortable-both';
        }
    }

}
