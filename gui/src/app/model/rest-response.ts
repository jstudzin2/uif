import {Page} from './page';
import {EmbeddedJson} from './embedded-json';


export class RestResponse<T> {
    _embedded: EmbeddedJson<T>;
    page: Page;
}
