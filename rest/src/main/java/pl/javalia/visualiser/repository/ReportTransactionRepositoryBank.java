package pl.javalia.visualiser.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import pl.javalia.visualiser.model.ReportTransaction;
import pl.javalia.visualiser.model.ReportTransactionBank;

/**
 * Place description here.
 *
 * @author Q1JS@nykredit.dk
 */

@RepositoryRestResource(collectionResourceRel = "resources", path = "transactionsBank")
public interface ReportTransactionRepositoryBank extends PagingAndSortingRepository<ReportTransactionBank, Long> {

    @RestResource(exported = true)
    @Query("select reportTransaction from ReportTransactionBank reportTransaction where " +
            "LOWER(reportTransaction.groupId) like LOWER(:search||'%') or LOWER(reportTransaction.description) like LOWER(:search||'%')" +
            "or LOWER(reportTransaction.security) like LOWER(:search||'%') or LOWER(reportTransaction.reconId) like LOWER(:search||'%')" +
            " or LOWER(reportTransaction.manualId) like LOWER(:search||'%') or LOWER(reportTransaction.amount) like LOWER(:search||'%') " +
            "     or LOWER(TO_CHAR(reportTransaction.date, 'DDMMYYYY')) like LOWER(:search||'%') or LOWER(reportTransaction.portfolio) like LOWER(:search||'%')" +
            " or LOWER(reportTransaction.tt) like LOWER(:search||'%')  or LOWER(reportTransaction.mtt) like LOWER(:search||'%')  or LOWER(reportTransaction.skip) like LOWER(:search||'%')")
    Page<ReportTransactionBank> findAllLike(@Param("search") String search, Pageable pageable);

    @RestResource(exported = true)
    @Query("select reportTransaction from ReportTransactionBank reportTransaction where (reportTransaction.reconId is null or reportTransaction.reconId =0) and (" +
            "LOWER(reportTransaction.groupId) like LOWER(:search||'%') or LOWER(reportTransaction.description) like LOWER(:search||'%')" +
            "or LOWER(reportTransaction.security) like LOWER(:search||'%') or LOWER(reportTransaction.reconId) like LOWER(:search||'%')" +
            " or LOWER(reportTransaction.manualId) like LOWER(:search||'%') or LOWER(reportTransaction.amount) like LOWER(:search||'%') " +
            "    or LOWER(TO_CHAR(reportTransaction.date, 'DDMMYYYY')) like LOWER(:search||'%') or LOWER(reportTransaction.portfolio) like LOWER(:search||'%')" +
            " or LOWER(reportTransaction.tt) like LOWER(:search||'%')  or LOWER(reportTransaction.mtt) like LOWER(:search||'%')  or LOWER(reportTransaction.skip) like LOWER(:search||'%'))")
    Page<ReportTransactionBank> findAllLikeRecon(@Param("search") String search, Pageable pageable);
}