import {Component, OnInit} from '@angular/core';
import {ReportTransation} from './reportTransation';

import {Page} from '../model/page';
import {SortConfig} from '../model/sort-config';
import {ReportTransationService} from './reportTransaction.service';

@Component({
  selector: 'app-report-transaction',
  templateUrl: './report-transaction.component.html',
  styleUrls: ['./report-transaction.component.css']
})
export class ReportTransactionComponent implements OnInit {

  public reportTransations: ReportTransation[] = [];
  public recon: boolean = false;
  public query: string;
  public page: Page = null;
  public sortConfig = new SortConfig('id', this);
  public editCopy: ReportTransation;

  constructor(private reportTransationService: ReportTransationService) {
  }

  ngOnInit() {
    this.query = '';
    this.recon = true;
    this.getData();
  }

  getData(page?: number): void {
    this.reportTransationService.list(this.query, this.recon, page, this.sortConfig.field, this.sortConfig.direction)
      .subscribe(
        response => {
          this.reportTransations = [];
          const reportTransations = response._embedded ? response._embedded.resources : [];
          for (const reportTransation of reportTransations) {
            this.reportTransations.push(reportTransation);
          }
          this.page = response.page;
        }
      );
  }

  filterRecon(e) {
    this.recon = e.target.checked;
    this.getData();
  }

  edit(reportTransation: ReportTransation) {
    this.editCopy = new ReportTransation();
    this.editCopy.gnxFlag = reportTransation.gnxFlag;
    this.editCopy.manualId= reportTransation.manualId;
    this.editCopy.security= reportTransation.security;
    this.editCopy.groupId= reportTransation.groupId;
    this.editCopy.tt= reportTransation.tt;
    this.editCopy.mtt= reportTransation.mtt;
    this.editCopy.portfolio= reportTransation.portfolio;
    reportTransation.edition = true;
  }

  delete(reportTransation: ReportTransation) {
    reportTransation.gnxFlag = "H";
    this.save(reportTransation);
  }


  cancel(reportTransation: ReportTransation) {
    if (reportTransation.new) {
      this.reportTransations = this.reportTransations.filter((param) => !param.new);
    } else {
      reportTransation.gnxFlag = this.editCopy.gnxFlag;
      reportTransation.manualId = this.editCopy.manualId;
      reportTransation.security = this.editCopy.security;
      reportTransation.groupId  = this.editCopy.groupId;
      reportTransation.tt = this.editCopy.tt;
      reportTransation.mtt = this.editCopy.mtt;
      reportTransation.portfolio = this.editCopy.portfolio;
      reportTransation.edition = false;
    }
  }

  save(reportTransation: ReportTransation) {
    reportTransation.skip = "M";
    reportTransation.reconFlag = "M0";
    if (reportTransation.new) {
      this.reportTransationService.add(reportTransation).subscribe(() => this.getData(this.page.number));
    } else {
      this.reportTransationService.edit(reportTransation).subscribe(() => this.getData(this.page.number));
    }
  }

  add() {
    const reportTransation = new ReportTransation;
    reportTransation.edition = true;
    reportTransation.new = true;
    this.reportTransations.push(reportTransation);
  }

  changePage(toPage: string): void {
    if (toPage !== '...') {
      this.getData(+toPage - 1);
    }
  }

  numberWithCommas(x): string {
    if (x) {
      return x.toString().replace(",", ".");
    }
    return;
  }

  getPages(): string[] {
    const possibleChoices: string[] = ['1'];

    if (this.page.totalPages > 1) {
      if (this.page.totalPages > 5) {
        if (this.page.number > 1 && this.page.number < this.page.totalPages - 2) {
          const currentPage = this.page.number + 1;
          possibleChoices.push('...');
          possibleChoices.push(this.page.number.toString());
          possibleChoices.push(currentPage.toString());
          possibleChoices.push((currentPage + 1).toString());
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else if (this.page.number < this.page.totalPages - 2) {
          possibleChoices.push('2');
          possibleChoices.push('3');
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else {
          possibleChoices.push('2');
          possibleChoices.push('...');
          possibleChoices.push((this.page.totalPages - 2).toString());
          possibleChoices.push((this.page.totalPages - 1).toString());
          possibleChoices.push(this.page.totalPages.toString());
        }
      } else {
        for (let i = 1; i < this.page.totalPages; i++) {
          possibleChoices.push((i + 1).toString());
        }
      }
    }

    return possibleChoices;
  }
}
