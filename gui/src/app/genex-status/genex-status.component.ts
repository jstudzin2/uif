import {Component, OnInit} from '@angular/core';
import {GenexStatus} from './genexStatus';
import {Page} from '../model/page';
import {SortConfig} from '../model/sort-config';
import {GenexStatusService} from './genex-status.service';

@Component({
  selector: 'app-genex-status',
  templateUrl: './genex-status.component.html',
  styleUrls: ['./genex-status.component.css']
})
export class GenexStatusComponent implements OnInit {

  public genexStatuses: GenexStatus[] = [];

  public query:string;
  public page: Page = null;
  public sortConfig = new SortConfig('date', this);

  constructor(private genexStatusService: GenexStatusService) {
  }

  ngOnInit() {
    this.getData();
  }

  getData(page?: number): void {
    this.genexStatusService.list(this.query,page, this.sortConfig.field, this.sortConfig.direction)
      .subscribe(
        response => {
          this.genexStatuses = [];
          const genexStatuses = response._embedded ? response._embedded.resources : [];
          for (const genexStatus of genexStatuses) {
            this.genexStatuses.push(genexStatus);
          }
          this.page = response.page;
        }
      );
  }


  getPages(): string[] {
    const possibleChoices: string[] = ['1'];

    if (this.page.totalPages > 1) {
      if (this.page.totalPages > 5) {
        if (this.page.number > 1 && this.page.number < this.page.totalPages - 2) {
          const currentPage = this.page.number + 1;
          possibleChoices.push('...');
          possibleChoices.push(this.page.number.toString());
          possibleChoices.push(currentPage.toString());
          possibleChoices.push((currentPage + 1).toString());
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else if (this.page.number < this.page.totalPages - 2) {
          possibleChoices.push('2');
          possibleChoices.push('3');
          possibleChoices.push('...');
          possibleChoices.push(this.page.totalPages.toString());
        } else {
          possibleChoices.push('2');
          possibleChoices.push('...');
          possibleChoices.push((this.page.totalPages - 2).toString());
          possibleChoices.push((this.page.totalPages - 1).toString());
          possibleChoices.push(this.page.totalPages.toString());
        }
      } else {
        for (let i = 1; i < this.page.totalPages; i++) {
          possibleChoices.push((i + 1).toString());
        }
      }
    }

    return possibleChoices;
  }

  changePage(toPage: string): void {
    if (toPage !== '...') {
      this.getData(+toPage - 1);
    }
  }

}
