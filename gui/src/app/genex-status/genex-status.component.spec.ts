import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenexStatusComponent } from './genex-status.component';

describe('GenexStatusComponent', () => {
  let component: GenexStatusComponent;
  let fixture: ComponentFixture<GenexStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenexStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenexStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
