package pl.javalia.visualiser.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import pl.javalia.visualiser.model.GenexStatus;
import pl.javalia.visualiser.model.ProcessValidation;

/**
 * Place description here.
 *
 * @author Q1JS@nykredit.dk
 */

@RepositoryRestResource(collectionResourceRel = "resources", path = "validations")
public interface ProcessValidationRepository extends PagingAndSortingRepository<ProcessValidation, Long> {

    @RestResource(exported = true)
    @Query("select processValidation from ProcessValidation processValidation where " +
            "LOWER(processValidation.source) like LOWER(:search||'%') or LOWER(processValidation.name) like LOWER(:search||'%')" +
            " or LOWER(processValidation.status) like LOWER(:search||'%') ORDER BY processValidation.status")
    Page<ProcessValidation> findAllLike(@Param("search") String search, Pageable pageable);

}